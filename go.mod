module github.com/pojol/httpbot

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/uuid v1.1.2
	github.com/influxdata/influxdb v1.8.3
	github.com/mitchellh/mapstructure v1.3.3
	github.com/pmezard/go-difflib v1.0.0
)
